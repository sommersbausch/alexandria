﻿using System;
using System.ComponentModel;
using Newtonsoft.Json;
using Windows.Storage;
using Alexandria.Util;

namespace Alexandria
{
	// This file contains a stupid workaround because UWP does not support x:Static. In the application StaticResource list,
	// there is the instance of ApplicationSettingsResourceObject. This is the class that can be bound to in XAML, while the 
	// ApplicationSettings class is what the codebehind will work with to change the settings.
	// 
	// 
	// Seriously Microsoft?

	
	public static class ApplicationSettings
	{
		private static readonly string SETTINGS_FILE_NAME = "appsettings.json";

		public static bool IsAdmin {
			get { return ApplicationSettingsResourceObject.Instance.IsAdmin; }
			set { ApplicationSettingsResourceObject.Instance.IsAdmin = value; }
		}

		public static string EncryptedAdminPassword {
			get { return ApplicationSettingsResourceObject.Instance.EncryptedAdminPassword; }
		}
		public static EncryptedString RawEncryptedAdminPassword {
			get { return ApplicationSettingsResourceObject.Instance.RawEncryptedAdminPassword; }
		}

		public static void SaveFile()
		{
			Util.IOHelper.OpenLocalFile(SETTINGS_FILE_NAME, async (name, file) => {
				try
				{
					string serial = JsonConvert.SerializeObject(ApplicationSettingsResourceObject.Instance);
					await FileIO.WriteTextAsync(file, serial);
				}
				catch (Exception e)
				{
					UI.Dialog.PopupDialog.DisplayError("Could not serialize application settings. Reason: '" + e.Message + "'." + 
						" Changes to application settings will not persist.");
				}
			}, (name, ex) => {
				UI.Dialog.PopupDialog.DisplayError("Could not save application settings file. Reason: '" + ex.Message + "'.");
			}, true);
		}

		public static void LoadFromFile()
		{
			Util.IOHelper.OpenLocalFile(SETTINGS_FILE_NAME, async (name, file) => {
				try
				{
					string serial = await FileIO.ReadTextAsync(file);
					JsonConvert.PopulateObject(serial, ApplicationSettingsResourceObject.Instance);
				}
				catch (Exception e)
				{
					UI.Dialog.PopupDialog.DisplayError("Could not deserialize application settings. Reason: '" + e.Message + "'." +
						" Application settings will be set to defaults.");
				}
			}, (name, ex) => {
				UI.Dialog.PopupDialog.DisplayError("Could not load application settings file. Reason: '" + ex.Message + "'. " +
					"Creating default settings. Default Administrator password: 'password'.");
				// Silently attempt to create a new default settings file
				Util.IOHelper.CreateLocalFile(SETTINGS_FILE_NAME, async (name2, file) => {
					string serial = JsonConvert.SerializeObject(ApplicationSettingsResourceObject.Instance);
					await FileIO.WriteTextAsync(file, serial);
				}, null);
			}, false);
		}
	}

	[JsonObject(MemberSerialization = MemberSerialization.OptIn)]
	public class ApplicationSettingsResourceObject : INotifyPropertyChanged
	{ 
		public static ApplicationSettingsResourceObject Instance { get; private set; } = null;
		public event PropertyChangedEventHandler PropertyChanged;

		private bool _isAdmin = false;
		public bool IsAdmin {
			get { return _isAdmin; }
			set {
				_isAdmin = value;
				propertyChanged("IsAdmin");
			}
		}

		private EncryptedString _encryptedAdminPassword = EncryptedString.CreateFromNonEncryptedString("password");
		[JsonProperty(PropertyName = "password")]
		public string EncryptedAdminPassword {
			get { return _encryptedAdminPassword.ToString(); }
			set {
				_encryptedAdminPassword = EncryptedString.CreateFromEncryptedString(Convert.FromBase64String(value));
				propertyChanged("EncryptedAdminPassword");
			}
		}
		public EncryptedString RawEncryptedAdminPassword
		{
			get { return _encryptedAdminPassword; }
		}

		public ApplicationSettingsResourceObject()
		{
			Instance = this;
		}

		private void propertyChanged(string pName)
		{
			var tmp = PropertyChanged; // Temporary assignment to alleviate possible data races
			tmp?.Invoke(this, new PropertyChangedEventArgs(pName));
		}

		public ApplicationSettingsResourceObject Clone()
		{
			return MemberwiseClone() as ApplicationSettingsResourceObject;
		}

		public bool SettingsEqual(ApplicationSettingsResourceObject other)
		{
			// TODO: Add fields to compare as we go
			return (_encryptedAdminPassword == other._encryptedAdminPassword);
		}
	}
}
