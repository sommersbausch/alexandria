﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace Alexandria.UI.Pages
{
	public sealed partial class AppSettingsPage : Page
	{
		private readonly ApplicationSettingsResourceObject savedSettings = null;

		public AppSettingsPage()
		{
			this.InitializeComponent();

			savedSettings = ApplicationSettingsResourceObject.Instance.Clone();
		}

		private void CancelChanges_Click(object sender, RoutedEventArgs e)
		{
			if (!savedSettings.SettingsEqual(ApplicationSettingsResourceObject.Instance))
			{
				Dialog.ConfirmationDialog.Display(Dialog.ConfirmationType.YesNo, "There are unsaved changes. Discard them and go back?", 
					(type, result) => {
						if (result) this.Frame.GoBack();
					});
			}
			else
				this.Frame.GoBack();
		}

		private void SaveChanges_Click(object sender, RoutedEventArgs e)
		{
			if (!savedSettings.SettingsEqual(ApplicationSettingsResourceObject.Instance))
			{
				Dialog.ConfirmationDialog.Display(Dialog.ConfirmationType.YesNo, "Overwrite old settings?",
					(type, result) => {
						if (result)
						{
							ApplicationSettings.SaveFile();
							this.Frame.GoBack();
						}
					});
			}
			else
			{
				Dialog.PopupDialog.DisplayInfo("No settings were changed.");
				ApplicationSettings.SaveFile();
				this.Frame.GoBack();
			}
		}
	}
}
