﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using ADialog = Alexandria.UI.Dialog;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Alexandria.UI.Pages
{
    public sealed partial class MainPage : Page
    {
		private static bool firstLoad = true;

        public MainPage()
        {
            this.InitializeComponent();

			this.Loaded += (_, e) =>
			{
				if (firstLoad)
				{
					ApplicationSettings.LoadFromFile();
				}
				firstLoad = false;
			};
        }

		private void CheckoutButton_Click(object sender, RoutedEventArgs e)
		{
			ADialog.PopupDialog.DisplayError("Checkout functionality not yet implemented.");
		}

		private void CheckinButton_Click(object sender, RoutedEventArgs e)
		{
			ADialog.PopupDialog.DisplayError("Checkin functionality not yet implemented.");
		}

		#region Admin Flyout Button Handlers
		private void AdminFlyoutLogin_Click(object sender, RoutedEventArgs e)
		{
			if (ApplicationSettings.IsAdmin)
			{
				ADialog.ConfirmationDialog.Display(ADialog.ConfirmationType.YesNo, "Log out from Administrator access?", (type, result) => {
					if (result)
					{
						ADialog.PopupDialog.DisplayInfo("Successfully logged out of administrator account.");
						ApplicationSettings.IsAdmin = false;
					}
				});
			}
			else
			{
				ADialog.PasswordPrompt.Display(ApplicationSettings.RawEncryptedAdminPassword, (password, result) => {
					if (result == ADialog.PasswordPromptResult.Correct)
					{
						ADialog.PopupDialog.DisplayInfo("You have successfully logged in as the Administrator.\n" + 
							"Please remember to log out when you are done making changes.");
						ApplicationSettings.IsAdmin = true;
					}
					else if (result == ADialog.PasswordPromptResult.Incorrect)
					{
						ADialog.PopupDialog.DisplayError("Password entered for Administrator access was incorrect.");
						ApplicationSettings.IsAdmin = false;
					}
				});
			}
		}

		private void AdminFlyoutAppSettings_Click(object sender, RoutedEventArgs e)
		{
			this.Frame.Navigate(typeof(AppSettingsPage));
		}

		private void AdminFlyoutDatabaseSettings_Click(object sender, RoutedEventArgs e)
		{
			this.Frame.Navigate(typeof(DatabaseSettingsPage));
		}

		private void AdminFlyoutEditDatabase_Click(object sender, RoutedEventArgs e)
		{
			this.Frame.Navigate(typeof(DatabaseEditPage));
		}
		#endregion // Admin Flyout Button Handlers
	}
}
