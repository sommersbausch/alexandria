﻿using System;
using Windows.UI.Xaml.Data;

namespace Alexandria.UI.Converters
{
	public class LoginButtonTextConverter : IValueConverter
	{
		public LoginButtonTextConverter() { }

		object IValueConverter.Convert(object value, Type targetType, object parameter, string language)
		{
			if ((bool)value)
				return "Logout";
			else
				return "Login";
		}

		// This function will never be used, because it is a one way binding
		object IValueConverter.ConvertBack(object value, Type targetType, object parameter, string language)
		{
			return false;
		}
	}
}
