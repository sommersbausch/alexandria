﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Alexandria.UI.Dialog
{
	public static class PopupDialog
	{
		private static ContentDialog savedDialog = null;

		public static void ensureClosed()
		{
			savedDialog?.Hide();
		}

		private static void populateDialog(string title, string content)
		{
			if (savedDialog == null)
			{
				savedDialog = new ContentDialog
				{
					Title = "",
					Width = 400
				};
				savedDialog.PrimaryButtonText = "Ok";
				savedDialog.IsSecondaryButtonEnabled = false;

				savedDialog.Content = new TextBlock
				{
					Text = "",
					TextWrapping = TextWrapping.Wrap,
					Margin = new Thickness(0, 20, 0, 0),
					Width = 350
				};
			}

			ensureClosed();
			ConfirmationDialog.ensureClosed();

			savedDialog.Title = title;
			(savedDialog.Content as TextBlock).Text = content;
		}

		public async static void DisplayError(string content)
		{
			populateDialog("Error", content);
			await savedDialog.ShowAsync();
		}

		public async static void DisplayWarning(string content)
		{
			populateDialog("Warning", content);
			await savedDialog.ShowAsync();
		}

		public async static void DisplayInfo(string content)
		{
			populateDialog("Info", content);
			await savedDialog.ShowAsync();
		}
	}
}
