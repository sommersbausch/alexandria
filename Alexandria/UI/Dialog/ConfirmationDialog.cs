﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Alexandria.UI.Dialog
{
	public enum ConfirmationType
	{
		OkCancel,
		YesNo,
		AcceptDecline
	}

	public delegate void ConfirmationDialogResultCallback(ConfirmationType type, bool result);

	public static class ConfirmationDialog
	{
		private static ContentDialog savedDialog = null;

		public static void ensureClosed()
		{
			savedDialog?.Hide();
		}

		private static string getPrimaryText(ConfirmationType type)
		{
			switch (type)
			{
				case ConfirmationType.AcceptDecline:
					return "Accept";
				case ConfirmationType.OkCancel:
					return "Ok";
				case ConfirmationType.YesNo:
				default:
					return "Yes";
			}
		}

		private static string getSecondaryText(ConfirmationType type)
		{
			switch (type)
			{
				case ConfirmationType.AcceptDecline:
					return "Decline";
				case ConfirmationType.OkCancel:
					return "Cancel";
				case ConfirmationType.YesNo:
				default:
					return "No";
			}
		}

		public async static void Display(ConfirmationType type, string content, ConfirmationDialogResultCallback callback)
		{
			if (savedDialog == null)
			{
				savedDialog = new ContentDialog
				{
					Title = "Confirmation",
					Width = 400
				};

				savedDialog.Content = new TextBlock
				{
					Text = "",
					TextWrapping = TextWrapping.Wrap,
					Margin = new Thickness(0, 20, 0, 0)
				};
			}

			ensureClosed();
			PopupDialog.ensureClosed();

			(savedDialog.Content as TextBlock).Text = content;
			savedDialog.PrimaryButtonText = getPrimaryText(type);
			savedDialog.SecondaryButtonText = getSecondaryText(type);

			var result = await savedDialog.ShowAsync();
			savedDialog.Hide();
			callback?.Invoke(type, (result == ContentDialogResult.Primary));
		}
	}
}
