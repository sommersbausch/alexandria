﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Alexandria.Util;

namespace Alexandria.UI.Dialog
{
	public enum PasswordPromptResult
	{
		Correct,
		Incorrect,
		Canceled
	}

	public delegate void PasswordPromptResultCallback(string password, PasswordPromptResult result);
	public delegate void EncryptedPasswordPromptResultCallback(EncryptedString password, PasswordPromptResult result);

	public static class PasswordPrompt
	{
		public static async void Display(string password, PasswordPromptResultCallback callback)
		{
			var dialog = new ContentDialog {
				Title = "Enter Admin Password",
				Width = 500
			};
			dialog.PrimaryButtonText = "Ok";
			dialog.SecondaryButtonText = "Cancel";

			var passbox = new PasswordBox {
				Width = 450,
				HorizontalAlignment = HorizontalAlignment.Center,
				PasswordRevealMode = PasswordRevealMode.Hidden,
				BorderThickness = new Thickness(0),
				VerticalAlignment = VerticalAlignment.Center,
				VerticalContentAlignment = VerticalAlignment.Center
			};
			passbox.KeyUp += (sender, args) => {
				if (args.Key == Windows.System.VirtualKey.Enter)
					dialog.Hide();
			};
			dialog.Content = passbox;

			var result = await dialog.ShowAsync();
			if (result == ContentDialogResult.Primary || result == ContentDialogResult.None)
			{
				string pass = passbox.Password;
				// TODO: Encrypt this and allow loading from a settings file
				callback?.Invoke(password, (pass == password) ? PasswordPromptResult.Correct : PasswordPromptResult.Incorrect);
			}
			else
				callback?.Invoke(password, PasswordPromptResult.Canceled);
		}

		public static async void Display(EncryptedString password, EncryptedPasswordPromptResultCallback callback)
		{
			var dialog = new ContentDialog
			{
				Title = "Enter Admin Password",
				Width = 500
			};
			dialog.PrimaryButtonText = "Ok";
			dialog.SecondaryButtonText = "Cancel";

			var passbox = new PasswordBox
			{
				Width = 450,
				HorizontalAlignment = HorizontalAlignment.Center,
				PasswordRevealMode = PasswordRevealMode.Hidden,
				BorderThickness = new Thickness(0),
				VerticalAlignment = VerticalAlignment.Center,
				VerticalContentAlignment = VerticalAlignment.Center
			};
			passbox.KeyUp += (sender, args) => {
				if (args.Key == Windows.System.VirtualKey.Enter)
					dialog.Hide();
			};
			dialog.Content = passbox;

			var result = await dialog.ShowAsync();
			if (result == ContentDialogResult.Primary || result == ContentDialogResult.None)
			{
				string pass = passbox.Password;
				EncryptedString epass = EncryptedString.CreateFromNonEncryptedString(pass);
				callback?.Invoke(password, epass.Equals(password) ? PasswordPromptResult.Correct : PasswordPromptResult.Incorrect);
			}
			else
				callback?.Invoke(password, PasswordPromptResult.Canceled);
		}
	}
}
