﻿using System;
using System.IO;
using Windows.Storage;

namespace Alexandria.Util
{
	public delegate void FileOpenedCallback(string filename, StorageFile file);
	public delegate void FileOpenedFailureCallback(string filename, Exception e);

	public static class IOHelper
	{
		public static async void OpenLocalFile(string filename, FileOpenedCallback callback, FileOpenedFailureCallback ecallback, bool create)
		{
			StorageFolder localFolder = ApplicationData.Current.LocalFolder;

			try
			{
				StorageFile file = null;
				try
				{
					file = await localFolder.GetFileAsync(filename);
				}
				catch (FileNotFoundException) // The only surefire way to test if the file exists
				{
					if (create)
						file = await localFolder.CreateFileAsync(filename, CreationCollisionOption.ReplaceExisting);
					else
					{
						ecallback?.Invoke(filename, new Exception("The file \"" + filename + "\" could not be found in the Local folder"));
						return;
					}
				}

				callback?.Invoke(filename, file as StorageFile);
			}
			catch (Exception e)
			{
				ecallback?.Invoke(filename, e);
			}
		}

		public static async void CreateLocalFile(string filename, FileOpenedCallback callback, FileOpenedFailureCallback ecallback)
		{
			OpenLocalFile(filename, callback, ecallback, true);
		}
	}
}
