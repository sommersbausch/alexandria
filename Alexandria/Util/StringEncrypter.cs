﻿using System;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Security.Cryptography;
using Windows.Security.Cryptography.Core;
using Windows.Storage.Streams;
using System.Linq;

namespace Alexandria.Util
{
	public class EncryptedString
	{
		private readonly byte[] data;

		private EncryptedString(byte[] estring)
		{
			data = new byte[estring.Length];
			estring.CopyTo(data, 0);
		}

		public bool Equals(EncryptedString other)
		{
			return data.SequenceEqual(other.data);
		}

		public bool Equals(byte[] other)
		{
			return data.SequenceEqual(other);
		}

		public static EncryptedString CreateFromNonEncryptedString(string raw)
		{
			byte[] estring = StringEncrypter.EncryptString(raw);
			return new EncryptedString(estring);
		}

		public static EncryptedString CreateFromEncryptedString(byte[] estring)
		{
			return new EncryptedString(estring);
		}

		public override string ToString()
		{
			return Convert.ToBase64String(data);
		}
	}

	// This class uses the well established and tested PBKDF2 algorithm, and uses the string
	// as its own salt. This is not the most secure option ever, but it does provide a very decent
	// level of security, and we dont expect this system to be the target of any hacks.
	public static class StringEncrypter
	{
		private static readonly KeyDerivationAlgorithmProvider ALG_PROVIDER 
			= KeyDerivationAlgorithmProvider.OpenAlgorithm(KeyDerivationAlgorithmNames.Pbkdf2Sha256);
		private static readonly uint ALG_ITERATIONS = 10000;

		public static byte[] EncryptString(string raw)
		{
			if (String.IsNullOrWhiteSpace(raw))
				throw new ArgumentException("A null or whitespace only string cannot be encrypted.", "raw");

			IBuffer rawBuff = CryptographicBuffer.ConvertStringToBinary(raw, BinaryStringEncoding.Utf8);
			CryptographicKey key = ALG_PROVIDER.CreateKey(rawBuff);

			IBuffer salt = CryptographicBuffer.ConvertStringToBinary(raw, BinaryStringEncoding.Utf8);
			KeyDerivationParameters kdparams = KeyDerivationParameters.BuildForPbkdf2(salt, ALG_ITERATIONS);

			byte[] derived = CryptographicEngine.DeriveKeyMaterial(key, kdparams, 32).ToArray();
			byte[] ret = new byte[derived.Length];
			derived.CopyTo(ret, 0);
			return ret;
		}
	}
}
