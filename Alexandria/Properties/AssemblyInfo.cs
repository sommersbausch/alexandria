﻿using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Alexandria")]
[assembly: AssemblyDescription("A simple kiosk application designed for small libraries, that runs on Windows 10 IoT Core.")]
#if DEBUG
[assembly: AssemblyConfiguration("Debug")]
#else
[assembly: AssemblyConfiguration("Release")]
#endif // DEBUG
[assembly: AssemblyCompany("Sommers-Bausch Observatory")]
[assembly: AssemblyProduct("Alexandria")]
[assembly: AssemblyCopyright("Copyright © Sommers-Bausch Observatory 2016")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("0.0.*")]
[assembly: ComVisible(false)]