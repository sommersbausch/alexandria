﻿using System;

namespace Alexandria.Database
{
	/// <summary>
	/// This class is a singleton instance that provides access to the LiteDB database. All data passing
	/// and object manipulation of the database *MUST* be done through this class to maintain coherency.
	/// </summary>
	public class DBAP : IDisposable
	{
		public static DBAP Instance { get; private set; } = null;

		#region Properties

		public bool IsDisposed { get; private set; } = false;
		#endregion // Properties

		public DBAP()
		{
			if (Instance != null)
			{

			}
		}
		~DBAP()
		{
			dispose(false);
		}

		#region Disposal
		public void Dispose()
		{
			dispose(true);
			GC.SuppressFinalize(this);
		}

		private void dispose(bool disposing)
		{

		}
		#endregion // Disposal
	}
}
