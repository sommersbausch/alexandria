# Alexandria (Library Kiosk Software)

Alexandria is a simple library kiosk program designed with small and medium self-checkout libraries in mind. It was designed to be an alternative to the prohibitively expansive and complicated library systems that are readily found online.

Alexandria is developed by the Sommers-Bausch Observatory Staff at the University of the Colorado at Boulder, and is copyright of the same group. It is licensed under the GNU GPLv3 license to allow for free use by any group or institution.

To submit issues or bug reports, please use the issue tracker at the BitBucket site for this project ([here](https://bitbucket.org/sommersbausch/alexandria/issues)). For quick questions about use, comments, or concerns,  please email [sean.moss@colorado.edu](sean.moss@colorado.edu).


## Planned Features

* Admin support for adding and removing books from the system inventory.
* Book association with barcodes. Most barcode formats are supported.
* Support for handheld barcode scanners.
* Self-Checkout and Checkin for users.
* Clean and minimalistic interface to support small screens and performance on Raspberry Pi 2/3.
* Touch screen support (limited by Win10 IoT Core touchscreen driver support)


## Contributors

The contributors to this project, in no particular order:

* Sean Moss